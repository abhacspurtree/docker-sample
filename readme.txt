#### Docker sample with Node.js ####

1. Install Express and Nodemon dependencies
    npm install

    (Edit api response in app.js)
2. Build docker image
    docker build -t node-docker-sample-1 .
3. Run docker image on port 9000 mapped to internal port 3000
    docker run -d -p 9000:3000 node-docker-sample-1

    (Launch localhost:9000 in browser to view api response)
4. Re-edit api response in app.js. Build new docker image
    docker build -t node-docker-sample-2 .
5. Run new docker image on port 9001 mapped to internal port 3000
    docker run -d -p 9001:3000 node-docker-sample-2

    (Launch localhost:9001 in browser to view api response)
6. List docker images created with process details
    docker ps
7. List docker images created   
    sudo docker images
8. Run a docker image with repository name & tag name   
    sudo docker run -d node-docker-sample-1:latest

    (Launch localhost:9000 (port number from process details) in browser to view api response)

Error handling:
1. Error - Error response from daemon: driver failed programming external connectivity on endpoint hardcore_leakey (cac8d48257edf91b1a4d2c5974015edff73eee493aa1d916a4dea926f011966e): Bind for 0.0.0.0:9000 failed: port is already allocated.
   Solution - list docker containers (docker ps)
   Kill the container running on specified port (docker kill containerId)
2. Kill all docker containers 
    docker kill $(docker ps -q) 