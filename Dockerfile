FROM node:9-slim
# specify the directory for application code
WORKDIR /app
# copy package.json file to app directory
COPY package.json /app
# install express dependency
RUN npm install
# copy the application to app directory
COPY . /app
# npm start to start the application
CMD ["npm", "start"]